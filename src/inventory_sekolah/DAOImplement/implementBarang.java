/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory_sekolah.DAOImplement;
import java.util.List;
import inventory_sekolah.model.*;
/**
 *
 * @author Lenovo
 */
public interface implementBarang {
    public void insert(barang b);

    public void update(barang b);

    public void delete(int id);

    public List<barang> getALL();

    public List<barang> getCariNama(String nama);
}
