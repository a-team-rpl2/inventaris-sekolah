/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory_sekolah.DAO;
import inventory_sekolah.koneksi.koneksi;
import inventory_sekolah.model.barang;
import inventory_sekolah.DAOImplement.implementBarang;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Lenovo
 */
public class daoBarang implements implementBarang {
    Connection connection;
    final String insert = "INSERT INTO tblbarang (nama, kategori,stok, deskripsi) VALUES (?, ?, ?,?);";
    final String update = "UPDATE tblbarang set nama=?, kategori=?, stok=?, deskripsi=? where id=? ;";
    final String delete = "DELETE FROM tblbarang where id=? ;";
    final String select = "SELECT * FROM tblbarang;";
    final String carinama = "SELECT * FROM tblbarang where nama like ?";
    
    public daoBarang() {
        connection = koneksi.connection();
    }

    public void insert(barang b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getNama());
            statement.setString(2, b.getKategori());
            statement.setString(3, b.getStok());
            statement.setString(4, b.getDeskripsi());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId(rs.getInt(1));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(barang b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getNama());
            statement.setString(2, b.getKategori());
            statement.setString(3, b.getStok());
            statement.setString(4, b.getDeskripsi());
            statement.setInt(5, b.getId());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<barang> getALL() {
        List<barang> lb = null;
        try {
            lb = new ArrayList<barang>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                barang b = new barang();
                b.setId(rs.getInt("id"));
                b.setNama(rs.getString("nama"));
                b.setKategori(rs.getString("kategori"));
                b.setStok(rs.getString("stok"));
                b.setDeskripsi(rs.getString("deskripsi"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoBarang.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

    public List<barang> getCariNama(String nama) {
        List<barang> lb = null;
        try {
            lb = new ArrayList<barang>();
            PreparedStatement st = connection.prepareStatement(carinama);
            st.setString(1, "%" + nama + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                barang b = new barang();
                b.setId(rs.getInt("id"));
                b.setNama(rs.getString("nama"));
                b.setKategori(rs.getString("kategori"));
                b.setStok(rs.getString("stok"));
                b.setDeskripsi(rs.getString("deskripsi"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoBarang.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
}
