/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory_sekolah.controller;
import inventory_sekolah.DAO.daoBarang;
import inventory_sekolah.DAOImplement.implementBarang;
import inventory_sekolah.model.barang;
import inventory_sekolah.model.tableModelBarang;
import inventory_sekolah.view.FormBarang;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Lenovo
 */
public class controllerBarang {
    FormBarang frame;
    implementBarang implBarang;
    List<barang> lb;

    public controllerBarang(FormBarang frame) {
        this.frame = frame;
        implBarang = new daoBarang();
        lb = implBarang.getALL();
    }

    //mengosongkan field
    public void reset() {
        frame.getTxtID().setText("");
        frame.getTxtBarang().setText("");
        frame.getTxtStok().setText("");
        frame.getTxtKategori().setSelectedItem("");
        frame.getTxtDeskripsi().setText("");
        
    }

    //menampilkan data ke dalam tabel
    public void isiTable() {
        lb = implBarang.getALL();
        tableModelBarang tmb = new tableModelBarang(lb) {};
        frame.getTabelData().setModel(tmb);
    }

    //merupakan fungsi untuk menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        frame.getTxtID().setText(lb.get(row).getId().toString());
        frame.getTxtBarang().setText(lb.get(row).getNama());
        frame.getTxtStok().setText(lb.get(row).getStok());
        frame.getTxtKategori().setSelectedItem(lb.get(row).getKategori());
        frame.getTxtDeskripsi().setText(lb.get(row).getDeskripsi());
    }

    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtBarang().getText().trim().isEmpty()& !frame.getTxtStok().getText().trim().isEmpty()) {
          
        barang b = new barang();
        b.setNama(frame.getTxtBarang().getText());
        b.setStok(frame.getTxtStok().getText());
        b.setKategori(frame.getTxtKategori().getSelectedItem().toString());
        b.setDeskripsi(frame.getTxtDeskripsi().getText());

        implBarang.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    //berfungsi untuk update data berdasarkan inputan user dari textfield di frame
    public void update() {
   if (!frame.getTxtID().getText().trim().isEmpty()) {
             
        barang b = new barang();
        b.setNama(frame.getTxtBarang().getText());
        b.setStok(frame.getTxtStok().getText());
        b.setKategori(frame.getTxtKategori().getSelectedItem().toString());
        b.setDeskripsi(frame.getTxtDeskripsi().getText());
        b.setId(Integer.parseInt(frame.getTxtID().getText()));
        implBarang.update(b);
        
        JOptionPane.showMessageDialog(null, "Update Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di ubah");
        }
    }

    //berfungsi menghapus data yang dipilih
    public void delete() {
        if (!frame.getTxtID().getText().trim().isEmpty()) {
            int id = Integer.parseInt(frame.getTxtID().getText());
            implBarang.delete(id);
            
            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
        }
    }

    public void isiTableCariNama() {
        lb = implBarang.getCariNama(frame.getTxtCariNama().getText());
        tableModelBarang tmb = new tableModelBarang(lb) {};
        frame.getTabelData().setModel(tmb);
    }

    public void carinama() {
        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
            implBarang.getCariNama(frame.getTxtCariNama().getText());
            isiTableCariNama();
        } else {
            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
        }
    }
}
