/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory_sekolah.model;
import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author Lenovo
 */
public abstract class tableModelBarang extends AbstractTableModel {
     List<barang> lb;

    public tableModelBarang(List<barang> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Nama";
            case 2:
                return "Stok";
            case 3:
                return "Kategori";
            case 4:
                return "Deskripsi";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getId();
            case 1:
                return lb.get(row).getNama();
            case 2:
                return lb.get(row).getStok();
            case 3:
                return lb.get(row).getKategori();
            case 4:
                return lb.get(row).getDeskripsi();
            default:
                return null;
        }
    }
}
